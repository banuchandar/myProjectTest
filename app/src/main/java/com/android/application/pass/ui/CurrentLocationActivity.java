package com.android.application.pass.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.application.pass.service.spacePassService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.android.application.pass.R;

/**
 *  Created by banu on 12/29/17.
 * CurrentLocationActivity - Here we are implementing Google API Client for location services, user location is fetched.
 * Any location changes are also captured and returned.
 */

public class CurrentLocationActivity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    //Member Variables
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private TextView latitude,longitude,altitude;
    Button submit;
    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
    private static final int REQUEST_LOCATION = 100;
    private Context context;
    private double fusedLatitude = 0.0;
    private  double fusedLongitude = 0.0;
    private  double fusedAltiitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        context = getApplicationContext();
        initViews();
        if(checkPlayServices()) {
            // Checking if user permissions are obtained.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // Granting permissions in Run Time
                ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION);
            } else {
                // permission has been granted, continue as usual
                fusedLocationInit();
                initRequestUpdate(this);
            }
        }else{
              //No Google play services available
        }
    }

    /*
    * API to initialize Views
    * */
    private void initViews() {
        latitude = (TextView) findViewById(R.id.latitudeValue);
        longitude = (TextView) findViewById(R.id.longitudeValue);
        altitude = (TextView) findViewById(R.id.altitudeValue);
        submit = (Button) findViewById(R.id.button);
        submit.setOnClickListener(this);
    }



    // check if google play services is installed on the device
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),"This device is support googe play service. Please download", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),"This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Call back method for user permission action
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the Fused location API we requested
                fusedLocationInit();
                initRequestUpdate(this);
            } else {
                // Permission was denied or request was cancelled
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Disconnect from API onStop()
        stopFusedLocation();
    }

    /*
    * Button click Handling
    * */
    public void onClick(View v){
        if(v.getId() == R.id.button){
            if((latitude.getText()==null) || (longitude.getText()==null) || (altitude.getText()==null)) {
                Toast.makeText(this, "Fields cannot be Null", Toast.LENGTH_LONG).show();
            }
            else{
                final String lat = latitude.getText().toString();
                final String lon = longitude.getText().toString();
                String[] myTaskParams = {lat, lon};
                spacePassService msCall = new spacePassService(CurrentLocationActivity.this);
                msCall.execute(myTaskParams);
            }

        }
    }

    /*
    * API to initialize and conect GoogleApiClient obj
    * */
    public void fusedLocationInit() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }

    /*
    * Method to disconnect the googleapiclient when leaving the activity
    * */
    public void stopFusedLocation() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /*
    * API to initialize set properties for Location services for the locatio update request obj
    * */
    public void initRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        mGoogleApiClient.connect();
                    }
                    initRequestUpdate(listener);
                }
            }
        }, 1000);
    }

    /*
  * API to return is Gogleapiclient is connected
  * */
    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());
        setFusedAltitude(location.getAltitude());

        latitude.setText(String.valueOf(getFusedLatitude()));
        longitude.setText(String.valueOf(getFusedLongitude()));
        altitude.setText(String.valueOf(getFusedAltitude()));
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

    public void setFusedAltitude(double alt) {
        fusedAltiitude = alt;
    }

    public double getFusedAltitude() {
        return fusedAltiitude;
    }
}

