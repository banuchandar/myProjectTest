package com.android.application.pass.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.application.pass.datamodel.ResponseParser;
import com.android.application.pass.R;
import com.android.application.pass.service.spacePassService;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by banu on 12/29/17.
 */

public class PassListAdapter extends RecyclerView.Adapter<PassListAdapter.ViewHolder>{

    private ResponseParser.Response[] list;
    public static List<String> timeDuration;
    public static List<String> passTime;

    PassListAdapter(ResponseParser.Response[] response){
        this.list = response;
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.passlist_view,parent,false);
        try {
            listUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.duration.setText(timeDuration.get(position) + " Seconds");
        holder.riseTime.setText("Time : " + passTime.get(position));
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView duration, riseTime;

        public ViewHolder(View itemView){
            super(itemView);
            duration = (TextView)itemView.findViewById(R.id.timeduration);
            riseTime = (TextView)itemView.findViewById(R.id.passTime);
        }
    }

    /**
     * convertDate - This method is used to convert system time in seconds to a user readable date and time
     * @param timeInMilli
     * @return Date is retutned as String
     */

    public String convertDate(long timeInMilli){
        String returnDate;
        SimpleDateFormat sDate = new SimpleDateFormat("MMM d, yyyy, EEE, hh:mm aaa");
        //Timezone is converted to GMT for this example.
        sDate.setTimeZone(TimeZone.getTimeZone("GMT"));
        returnDate = sDate.format(new Date(timeInMilli*1000L));
        return returnDate;
    }

    /**
     * listUpdate - This method is used to obtain the response object from JSON response and obtain duration and rise time for each pass.
     * The duration and risetime are then saved in an Array list to render to Recycler View
     * @throws JSONException
     */

    public void listUpdate() throws JSONException {
        ResponseParser.Response[] response = spacePassService.response;
        timeDuration = new ArrayList<String>();
        passTime = new ArrayList<String>();
        for (int i=0;i<getItemCount();i++){
            String duration = null;
            String riseTime = null;
            try {
                duration = String.valueOf(response[i].getDuration());
                riseTime = convertDate(response[i].getRisetime());
                timeDuration.add(duration);
                passTime.add(riseTime);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
