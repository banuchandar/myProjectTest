package com.android.application.pass.datamodel;

/**
 * Created by banu on 12/29/17.
 */

public final class ResponseParser {
    public final String message;
    public final Request request;
    public final Response[] response;

    public ResponseParser(String message, Request request, Response[] response){
        this.message = message;
        this.request = request;
        this.response = response;
    }

    public static final class Request {
        public final long altitude;
        public final long datetime;
        public final double latitude;
        public final double longitude;
        public final long passes;

        public Request(long altitude, long datetime, double latitude, double longitude, long passes){
            this.altitude = altitude;
            this.datetime = datetime;
            this.latitude = latitude;
            this.longitude = longitude;
            this.passes = passes;
        }
    }

    public static final class Response {
        public long duration;
        public long risetime;


        public Response(long duration, long risetime){
            super();
            this.duration = duration;
            this.risetime = risetime;
        }

        public long getDuration() {
            return duration;
        }

        public void setDuration(long duration) {
            this.duration = duration;
        }

        public long getRisetime() {
            return risetime;
        }

        public void setRisetime(long risetime) {
            this.risetime = risetime;
        }
    }
}