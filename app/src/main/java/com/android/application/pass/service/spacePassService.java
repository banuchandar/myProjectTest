package com.android.application.pass.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.android.application.pass.datamodel.ResponseParser;
import com.android.application.pass.ui.PassListActivity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by banu on 12/29/17.
 * This class is created to make API call to fetch data
 */
    public class spacePassService extends AsyncTask<String, String, ResponseParser> {

    // Member Variables
    private Context asyncContext;
    ResponseParser jSON = null;
    public static ResponseParser.Response[] response;
    private ProgressDialog progressDialog;
    String endPoint = "http://api.open-notify.org/iss-pass.json?";

    /**
     * spacePassService Constructor
     * @param context
     */
    public spacePassService(Context context){
        asyncContext = context;

    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(asyncContext);
        progressDialog.setMessage("Fetching data ...");
        progressDialog.show();
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                progressDialog.cancel();
            }
        });
    }

    @Override
    protected ResponseParser doInBackground(String... strings) {
        endPoint = endPoint+"&lat="+strings[0]+"&lon="+strings[1];

        // Set up HTTP post
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(endPoint);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream iStream = urlConnection.getInputStream();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(iStream, "utf-8"), 8);
            Gson gson = new Gson();
            jSON = gson.fromJson(bReader,ResponseParser.class);

        } catch (Exception e) {
            Log.e("Exception", "Error: " + e.getMessage());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return jSON;
    }

    @Override
    protected void onPostExecute(ResponseParser jsonObject) {
        super.onPostExecute(jsonObject);
        //parse JSON data
        try {
            response = new ResponseParser.Response[0];
            response = jsonObject.response;
            this.progressDialog.dismiss();
            asyncContext.startActivity(new Intent(asyncContext, PassListActivity.class));
        } catch (Exception e) {
            Log.e("JSONException", "Error: " + e.toString());
        }

    }
}
