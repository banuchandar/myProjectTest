package com.android.application.pass.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.application.pass.datamodel.ResponseParser;
import com.android.application.pass.R;
import com.android.application.pass.service.spacePassService;

/**
 * Created by banu on 12/29/17.
 */

public class PassListActivity extends AppCompatActivity{

    private RecyclerView rvRecyclerView;
    private PassListAdapter lvAdapter;
    private ResponseParser.Response responseList;
    Context context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.data_layout);
        rvRecyclerView = (RecyclerView) findViewById(R.id.rvrecyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        rvRecyclerView.setLayoutManager(layoutManager);
        rvRecyclerView.setHasFixedSize(true);
        lvAdapter = new PassListAdapter((ResponseParser.Response[]) spacePassService.response);
        rvRecyclerView.setAdapter(lvAdapter);
        lvAdapter.notifyDataSetChanged();
    }
}
